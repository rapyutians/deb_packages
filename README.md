# Rapyuta Debian Packages repository
Files are divided into two different folders:

* __public:__ packages, public key, rosdep rules, and other configuration files required for the users of this repository.
* __private:__ configuration and debian folders for package generation.

Both folders have the corresponding README file with instructions regarding how to set the repository, install packages, update the repository, etc.

## Contributing a new package
The procedure is as follows:

* Create the debian folder in order to create your package following the versioning scheme detailed below.
* Once you are sure a package works, you should create a PR including the debian folder required (to be placed in `private/debian_folders/`), the instructions to build the package, and the source information (repository, version, etc).
* The maintainer will create then the Debian package and update the repository.

Info on creating packages available in our wiki: https://wiki.rapyuta-robotics.com/doku.php?id=engineering:software:debian_packaging

### Versioning scheme:

The pkg version is composed as follows: `version_release~source`

__`version`:__ Version number. If an official version number is available, use that, even if there are newer changes in the master. Otherwise, if a new release is distributed our packages will not be updated properly. If version number is not available, version number based on the date the source code is given. The date format is _YY.MM_ (e.g. 16.04 is April 2016). After the version number, a indication of the source origin follows.

__`release`:__ Release number. It is the Debian package version which can be used if the packaging went somehow wrong. The Debian package version number usually starts with 1 as soon as a new version is available.

__`source`__: Source of the package. Options:
* `official` indicates that this is a package that was in some way officially released (but not yet packaged). For example, CGAL 4.8 is officially released, but not yet packaged for Ubuntu 14.04.
* `git-xxxxxxx` indicates that the source comes from a git/hg/svn repository. Specifically, the first part indicates the repository source (in this case the official git repository) and the 7 letters afterwards are the first 7 letters of the commit hash.


#### Examples:

* `cgal_4.8-1~official` is the first version of a package done for CGAL 4.8 using the same source as the official CGAL 4.8
* `ortools-dev_16.04-2~git-938372b` is the second version of a package done from OR Tools of the release v2016-04 from the git repository, commit hash 938372b.

## More info
Debian packages conventions (we do not strictly follow them):
https://readme.phys.ethz.ch/documentation/debian_version_numbers/

### Maintainer ###
1. Javier V. Gomez (jvgomez@rapyuta-robotics.com)
2. Dominique Hunziker (dominique.hunziker@rapyuta-robotics.com)