## Rapyuta Debian packages

Make sure that ros is installed, check if roscore works. 
Installation instructions> http://wiki.ros.org/kinetic/Installation/Ubuntu


### Add support for https transport to apt (this is required for server/docker/minimal installs that lack the https transport plugin for apt)
* `sudo apt-get install apt-transport-https`
### Configure the repository to be used
* `sudo sh -c 'echo "deb [arch=amd64] https://bitbucket.org/rapyutians/deb_packages/raw/master/public `lsb_release -cs` main" > /etc/apt/sources.list.d/rapyuta.list'`
* `wget -O - https://bitbucket.org/rapyutians/deb_packages/raw/master/public/keyFile | sudo apt-key add -`
* `sudo apt-get update`

### ROSdep Rules
`rapyuta.yaml` includes customized ROSdep rules in order to use our packages. To use it, you have to:

* `sudo rosdep init`
* `sudo sh -c 'echo "yaml https://bitbucket.org/rapyutians/deb_packages/raw/master/public/rapyuta.yaml" > /etc/ros/rosdep/sources.list.d/00-rapyuta.list'`
* `rosdep update`
