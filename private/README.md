## Instructions for the repo maintainer

### Preliminaries

In order to setup GPG key and conf file, follow [this article](http://blog.jonliv.es/blog/2011/04/26/creating-your-own-signed-apt-repository-and-debian-packages/). In short:

* `gpg --gen-key` (RSA Key, add a passphrase, no comment)
* `gpg --armor --export <key ID> --output keyFile`
* Copy keyFile into the public folder of the repository.
* `sudo apt-get install reprepro dpkg-sig`


### Include a new package

When a .deb package is created using the developer's instructions and corresponding debian folder, two steps are required:

* __Sign the package__: `dpkg-sig --sign builder mypackage_0.1.2_amd64.deb`
* __Include the package to the APT repository__: IMPORTANT, the command has to be run where the `conf` folder is! `reprepro --ask-passphrase --outdir ../public -Vb . includedeb trusty mypackage_0.1.2_amd64.deb`
* __Do not forget!__ Update rosdep rules `public/rapyuta.yaml` and `README.md` with package info.


### Help using dpkg-sig and setting private keys:

Export private key: `gpg --export-secret-keys -a KEY-ID > FileName`
Import private key: `gpg --import path/to/FileName`
Use key as default: `gpg --default-key KEY-ID`

Reprepro checks the file `conf/distributions` to choose which key to use: `SignWith: <key-id>`

*  http://blog.packagecloud.io/eng/2014/10/28/howto-gpg-sign-verify-deb-packages-apt-repositories/
*  https://wiki.debian.org/SettingUpSignedAptRepositoryWithReprepro
*  http://irtfweb.ifa.hawaii.edu/~lockhart/gpg/gpg-cs.html