README
======
Reason: Latest version of the source code may not be released as a package for ros indigo. Three packages in one for simplicity.

This is a debian folder that builds a debian package out of the source package "octomap" which is required for map generation.

Code source: https://github.com/OctoMap/octomap.git, devel
Commit: 595cb83bfe862524ebb571c628486710794c7b04 Tue Jun 14 10:32:16 2016 +0200
Debian folder source: http://packages.ros.org/ros/ubuntu/ trusty/main amd64 Packages (source package)

This debian would be redundant when "ros-indigo-octomap" version 1.8.1 or higher is released.

The steps for creating the debian package is as follows :
1. Clone the "octomap" repository from "https://github.com/OctoMap/octomap.git"
2. Verify you are on branch "devel" and commit "595cb83bfe862524ebb571c628486710794c7b04 "
3. The debian package is to be created only for the source code "octomap"
4. Copy the debian folder to the "octomap" folder of the downloaded repository.
**NOTE:** Put this debian folder in the root of the octomap repository to build all three libraries (octomap, octovis, and dynamicEDT3D) into just one package. Otherwise, only octomap library will be included. Currently, done this way for simplicity.
5. In the repository folder run:
```
dpkg-buildpackage -b
```
6. The debian package should be created in the parent directory to the "octomap" repository folder.
7. Navigate to this parent directory and run:
```
sudo dpkg -i ros-indigo-octomap_1.8.0.1-1~official_amd64.deb
```
8. Verify if the right version is installed using:
```
apt-cache policy ros-indigo-octomap
```


