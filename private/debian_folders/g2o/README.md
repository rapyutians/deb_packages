README
======
Reason: Upstream version does not work with Rapyuta's current mapping source code.
    - Source: https://github.com/rapyuta/g2o/tree/devel
    - Commit: afed96460d815333fd5aae04186e4b1c25bc8360 Wed Feb 24 15:04:02 2016 +0900
        Checkinstall config:

            0 -  Maintainer: [ jvgomez@rapyuta-robotics.com ]
            1 -  Summary: [ Rapyuta's version of G2O library ]
            2 -  Name:    [ libg2o-dev ]
            3 -  Version: [ 16.05 ]
            4 -  Release: [ 1~git-afed964 ]
            5 -  License: [ BSD ]
            6 -  Group:   [ checkinstall ]
            7 -  Architecture: [ amd64 ]
            8 -  Source location: [ build ]
            9 -  Alternate source location: [  ]
            10 - Requires: [ cmake,libeigen3-dev ]
            11 - Provides: [ libg2o-dev ]
            12 - Conflicts: [  ]
            13 - Replaces: [  ]
