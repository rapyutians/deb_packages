README
======

Reason: Latest version of the plugin may not be released as a package for ros indigo.
    - Source: https://github.com/OctoMap/octomap_rviz_plugins.git, indigo-devel
    - Debian folder: http://packages.ros.org/ros/ubuntu/ trusty/main amd64 Packages (source package)
    - Commit: 44b3aa2bb13dc5a45fb0e794d439b94c79e691dc Mon Jun 13 18:54:16 2016 +0200

This is a debian folder that builds a debian package out of the source package "octomap_rviz_plugins" which is required for octomap rviz visualization.

This debian would be redundant when "ros-indigo-octomap-rviz-plugins" version 0.0.6 or higher is released.

The steps for creating the debian package is as follows :
1. Clone the "octomap_rviz_plugins" repository from "https://github.com/OctoMap/octomap_rviz_plugins.git"
2. Verify you are on branch "indigo-devel" and commit "44b3aa2bb13dc5a45fb0e794d439b94c79e691dc"
3. Copy the debian folder to this downloaded repository folder.
4. In the repository folder run
```
dpkg-buildpackage -b
```
5. The debian package should be created in the parent directory to the repository folder.
6. Navigate to the parent directory and run
```
sudo dpkg -i ros-indigo-octomap-rviz-plugins_0.0.5.1-1~git-44b3aa2_amd64.deb
```
7. Verify if the right version is installed using
```
apt-cache policy ros-indigo-octomap-rviz-plugins
```


