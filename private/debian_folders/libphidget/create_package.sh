#!/bin/sh

# Run this script in the same folder where the debian.tar.gz file is located.

# Preparation
LIB_VERSION=2.1.8.20151217
mkdir libphidget-packaging
cd libphidget-packaging

# Get source of library
wget --tries=2 http://www.phidgets.com/downloads/libraries/libphidget_${LIB_VERSION}.tar.gz
tar -xzvf libphidget_${LIB_VERSION}.tar.gz
cd libphidget-${LIB_VERSION}

# Conveniently download compressed debian
tar -xzvf ../../debian.tar.gz -C .

# Build
dpkg-buildpackage -b -us -uc

# Clean up
cd ..
mv libphidget*.deb ../
cd ..
rm -rf libphidget-packaging
