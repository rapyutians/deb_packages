# Phidget C library #

## Reason ##
They do not provide deb packages.

## Source ##
* Various versions of the library: http://www.phidgets.com/old_drivers.php
* Direct Link: http://www.phidgets.com/downloads/libraries/libphidget_2.1.8.20151217.tar.gz

## Required by ##
This package is required for the package `rr_docking_station_dac`in the repository `docking_station`. It provides methods to control all Phidget devices via USB.

## Instructions ##
Run `create_package.sh` script. It will do all the work for you (including downloading and extracting debian).

__NOTE:__ Run it script in the same folder where the debian.tar.gz file is located.

## Who to talk to ##
* Robert Simpson (robert.simpson@rapyuta-robotics.com)
* Javier V. Gomez (jvgomez@rapyuta-robotics.com)

