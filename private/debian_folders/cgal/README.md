
README
======
 Reason: The required version is not distributed on Ubuntu 14.04
- Source: cgal.org, release 4.8
- Debian folder: https://launchpad.net/debian/+source/cgal/4.7-2


Standard [buildpackage procedure](https://wiki.rapyuta-robotics.com/doku.php?id=engineering:software:debian_packaging)
```
dpkg-buildpackage -b
```

