README
======
Reason: Latest version of the source code may not be released as a package for ros indigo.
   - Source: https://github.com/flexible-collision-library/fcl/ master
   - Debian folder: http://packages.ros.org/ros/ubuntu/ trusty/main amd64 Packages (source package)
   - Commit: 3558c1e739278ef60b328aab68bb953b040ea273 Thu Jun 16 17:19:38 2016 +0200

The steps for creating the debian package is as follows :
1. Clone the "fcl" repository from "https://github.com/flexible-collision-library/fcl"
2. Verify you are on branch "master" and commit "3558c1e739278ef60b328aab68bb953b040ea273"
4. Copy the debian folder to the "fcl" folder of the downloaded repository.
```
dpkg-buildpackage -b
```
6. The debian package should be created in the parent directory to the "fcl" repository folder.
```


