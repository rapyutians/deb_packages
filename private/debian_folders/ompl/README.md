README
======
 Reason: The latest changes in OMPL not officially released for Ubuntu 14.04
	- Source: https://github.com/ompl/ompl master
    - Debian folder: https://launchpad.net/ubuntu/+source/ompl/1.0.0+ds2-1build2
	- Commit: c1bceb665fb09cc55cc7368040fb864a1de729ec Thu Mar 31 14:54:07 2016 -0500

The steps for creating the debian package is as follows :
1. Clone the "ompl" repository from "https://github.com/ompl/ompl"
2. Verify you are on branch "master" and commit "c1bceb665fb09cc55cc7368040fb864a1de729ec"
4. Copy the debian folder to the "ompl" folder of the downloaded repository.
```
dpkg-buildpackage -b
```
6. The debian package should be created in the parent directory to the "ompl" repository folder.
```


