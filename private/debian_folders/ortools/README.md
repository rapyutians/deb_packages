README
======
Reason: Google does not provide an ortools deb package.

This is a debian folder that builds a debian package out of an already built or-tools.

Steps:

- get Ubuntu 16.04 binaries:
`cd ~/Downloads && wget https://github.com/google/or-tools/releases/download/v7.0-beta.1/or-tools_ubuntu-16.04_v7.0.6150-beta.tar.gz`

- extract:
`tar -xvzf or-tools_ubuntu-16.04_v7.0.6150-beta.tar.gz`

- change directory into extracted folder:
`cd or-tools_Ubuntu-16.04-64bit_v7.0.6150-beta/`

- copy the debian folder from this repo into there, e.g.:
`cp -r ~/repositories/deb_packages/private/debian_folders/ortools/debian .`

- build the debian package:
`dpkg-buildpackage -b`

The debian package will then appear in the parent folder.

