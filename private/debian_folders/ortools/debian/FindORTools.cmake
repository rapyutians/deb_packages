# Author: wolf.vollprecht@rapyuta-robotics.com
# Hardcoded ortools paths according to where the debian package
# installs them

if( CMAKE_SIZEOF_VOID_P EQUAL 8 )
	add_definitions(-D__aarch64__=1)
else( CMAKE_SIZEOF_VOID_P EQUAL 8 )
	add_definitions(-D__aarch64__=0)
endif( CMAKE_SIZEOF_VOID_P EQUAL 8 )

find_library(ORTOOLS_LIBRARIES 
    ortools
    HINTS /usr/local/lib
)
if(NOT ORTOOLS_LIBRARIES)
    message(FATAL_ERROR "
        Google or-tools was not properly installed. Please verify that libortools.so is in /usr/local/lib"
    )
endif(NOT ORTOOLS_LIBRARIES)

find_path(ORTOOLS_INCLUDE_DIR 
    constraint_solver/constraint_solver.h
    HINTS /usr/local/include/ortools
)
if(NOT ORTOOLS_INCLUDE_DIR)
    message(FATAL_ERROR "
        Google or-tools headers not properly installed, specifically /usr/local/include/ortools/constraint_solver/constraint_solver.h"
    )
endif(NOT ORTOOLS_INCLUDE_DIR)
